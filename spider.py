#!/usr/bin/env python
from __future__ import print_function
import html5lib, urllib2, sys
from argparse import ArgumentParser
from urlparse import urlparse, urlunparse, urljoin
from urllib2 import HTTPError


ap = ArgumentParser("")
ap.add_argument("url", nargs="+")
args = ap.parse_args()

start_urls = args.url[:]
todo = args.url


def follow_url_p (url):
    for x in start_urls:
        if url.startswith(x):
            return True


def normalize (url):
    scheme, netloc, path, params, query, fragment = urlparse(url)
    return urlunparse(("http", netloc, path, None, None, None))

seen = set()

while todo:
    url = todo[0]
    todo = todo[1:]

    try:
        print (url, file=sys.stderr)
        data = urllib2.urlopen(url).read()
        seen.add(url)
        t = html5lib.parse(data, namespaceHTMLElements=False)

        for i in t.findall(".//*[@href]"):
            href = i.attrib.get("href")
            href = normalize(urljoin(url, href))
            if href not in todo and href not in seen:
                print (href.encode("utf-8"))
                seen.add(href)
                if follow_url_p(href):
                    todo.append(href)

        for i in t.findall(".//*[@src]"):
            href = i.attrib.get("src")
            href = normalize(urljoin(url, href))
            if href not in todo and href not in seen:
                print (href.encode("utf-8"))
                seen.add(href)
                if follow_url_p(href):
                    todo.append(href)
    except HTTPError as e:
        print (url, e, file=sys.stderr)

